﻿using MPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Laboratornaya_4
{
    enum Tags { ArrayLengthTag, ArrayTag, MaxValuetag };

    class Program
    {
        static void Main(string[] args)
        {
            using (new MPI.Environment(ref args))
            {
                Thread.Sleep(3000);
                Console.WriteLine(" Hello, World! from rank " + Communicator.world.Rank
                  + " (running on " + MPI.Environment.ProcessorName + ")");

                Intracommunicator comm = Communicator.world;
                int rank = comm.Rank;

                int rowLength = 5;
                if (rank == 0)
                {
                    string line;
                    List<int[]> elements = new List<int[]>();
                    StreamReader file = new StreamReader(@"matrix.txt");
                    bool isFirstLine = true;
                    int length = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        var numbers = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Int32.Parse(x)).ToArray();
                        if (isFirstLine)
                        {
                            isFirstLine = !isFirstLine;
                            length = numbers.Length;
                        }
                        else if (numbers.Length != length)
                        {
                            throw new ArgumentException("УААААЙ НЕПРАВИЛЬНО ВВЕЛ ЦИФРЫ!");
                        }
                        elements.Add(numbers);
                    }
                    file.Close();

                    int countWorkingThreads = comm.Size;

                    if (countWorkingThreads <= 1)
                    {
                        System.Environment.Exit(0);
                    }

                    int maxValueOfMatrix = Int32.MinValue;
                    int currentRankToSend = 0;

                    List<Task> allTasks = new List<Task>();
                    foreach (var matrixLine in elements)
                    {
                        if (++currentRankToSend > countWorkingThreads - 1)
                            currentRankToSend = 1;

                        comm.Send(matrixLine.Count(), currentRankToSend, (int)Tags.ArrayLengthTag);
                        comm.Send(matrixLine, currentRankToSend, (int)Tags.ArrayTag);

                        var task = Task.Run(() =>
                        {
                            int receivedFirstValue;
                            comm.Receive<int>(currentRankToSend, (int)Tags.MaxValuetag, out receivedFirstValue);

                            if (receivedFirstValue > maxValueOfMatrix)
                            {
                                maxValueOfMatrix = receivedFirstValue;
                            }
                        });
                        allTasks.Add(task);
                    }

                    Task.WaitAll(allTasks.ToArray(),5000);
                    /*
                    var firstPart = elements.Take((int)elements.ToArray().Length / 2).ToArray();
                    var secondPart = elements.ToArray().Skip((int)elements.ToArray().Length / 2).ToArray();

                    comm.Send(firstPart.Length, 1, (int)Tags.ArrayLengthTag);
                    comm.Send(secondPart.Length, 2, (int)Tags.ArrayLengthTag);

                    comm.Send(firstPart, 1, (int)Tags.ArrayTag);
                    comm.Send(secondPart, 2, (int)Tags.ArrayTag);

                    int firstMaxValue, secondMaxValue;
                    comm.Receive<int>(1, (int)Tags.MaxValuetag, out firstMaxValue);
                    comm.Receive<int>(2, (int)Tags.MaxValuetag, out secondMaxValue);

                    int maxValueOfAllNumbers = (firstMaxValue > secondMaxValue) ? firstMaxValue : secondMaxValue;
                    */
                    int counter = 0;
                    Console.WriteLine("=========================================");

                    foreach (var matrixLine in elements)
                    {
                        foreach (int number in matrixLine)
                        {
                            ++counter;
                            //if()
                            Console.Write(Math.Round(Convert.ToDecimal((double)number / maxValueOfMatrix), 2));
                            Console.Write(" ");
                        }
                        Console.Write(System.Environment.NewLine);
                    }
                }
                else
                {
                        int length = 0;
                        comm.Receive<int>(0, (int)Tags.ArrayLengthTag, out length);
                        Console.WriteLine(comm.Rank + " received length " + length);
                        int[] arrayOfHalfNumbers = new int[length];
                        comm.Receive<int>(0, (int)Tags.ArrayTag, ref arrayOfHalfNumbers);

                        int maxValue = Int32.MinValue;
                        for (int i = 0; i < arrayOfHalfNumbers.Length; i++)
                        {
                            if (arrayOfHalfNumbers[i] > maxValue)
                            {
                                maxValue = arrayOfHalfNumbers[i];
                            }
                        }
                        comm.Send(maxValue, 0, (int)Tags.MaxValuetag);
                        Console.WriteLine(comm.Rank + " send its max value " + maxValue);
                }
            }
        }
    }
}
